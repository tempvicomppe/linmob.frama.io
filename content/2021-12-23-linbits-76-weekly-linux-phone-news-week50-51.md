+++
title = "LinBits 76: Weekly Linux Phone news / media roundup (week 50/51)"
date = "2021-12-23T21:58:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","PinePhone Pro","GNUnet","Phosh","Purism","LINMOBapps",]
categories = ["weekly update"]
authors = ["peter"]
+++

_Another week passed:_ A new Squeekboard release, Dual Screen PinePhone thanks to 3D printing, Phosh on Yocto and Genode for PinePhone progress!

<!-- more --> _Commentary in italics._ 

### Software releases
* Squeekboard (the Phosh keyboard) has had another [release](https://gitlab.gnome.org/World/Phosh/squeekboard/-/merge_requests/513), adding layouts, a debug mode and better errors among other things.

### Worth noting
* /u/5ILVER: [What if a pinephone was a pinephone keyboard?](https://old.reddit.com/r/PinePhoneOfficial/comments/riryb4/what_if_a_pinephone_was_a_pinephone_keyboard/). _Note: This is just some (albeit impressive) 3D printing work at this point, software still needs to be figured out._
* /u/TheJackiMonster: [GNUnet Messenger for mobile Linux](https://old.reddit.com/r/pinephone/comments/rk5zyb/gnunet_messenger_for_mobile_linux/). _Great to see progress with this endeavor!_

### Worth reading
#### Software progress
* This Week in GNOME: [#23 Modernized Settings](https://thisweek.gnome.org/posts/2021/12/twig-23/). _My favorite this week is the GTK4/libadwaita implementation of Cawbird._
* Georges Stavracas: [GTK4ifying Settings](https://feaneron.com/2021/12/21/gtk4ifying-settings/).
* Marcus Lundblad: [Christmas Maps](http://ml4711.blogspot.com/2021/12/christmas-maps.html).
* Phoronix: [Mediatek MT8192 Kompanio 820 Display Support Staged For Linux 5.17](https://www.phoronix.com/scan.php?page=news_item&px=MT8192-DRM-Linux-5.17). _IIRC, this chip is going to be used in future Chrome OS hardware, in case you where wondering why there's decent Linux progress for a Mediatek SoC._

#### Apps
* Carl Schwan: [More KDE Apps](https://carlschwan.eu/2021/12/18/more-kde-apps/). _Carl's work is impressive!_

#### Interface overview
* TuxPhones: [Short guide to Linux phone desktops](https://tuxphones.com/mobile-linux-phone-desktop-environments-de-comparison-interfaces/). _Nice guide!_

#### Non-Linux PinePhone development
* Genodians: [Pine Fun – Display](https://genodians.org/nfeske/2021-12-21-pine-fun-display).

#### Tongue in Cheek
* Guido Günther for Purism: [We’re puttin’ the Band back together](https://puri.sm/posts/were-puttin-the-band-back-together/).

#### Plain Marketing Year in Review
* Purism: [Year End Update for 2021, Looking Forward to 2022](Year End Update for 2021, Looking Forward to 2022)

#### PinePhone First Impressions
* DistroWatch: [The PinePhone running Manjaro and Plasma Mobile](https://distrowatch.com/weekly.php?issue=20211220#pinephone).

### Worth listening
* postmarketOS cast: [#12 v21.12, TTYescape, Mainline OP6/SHIFT6mq/Fairphones, pmbootstrap](https://cast.postmarketos.org/episode/12-v2112-TTYescape-Mainline-OP6-SHIFT6mq-Fairphones-pmb/). _Another great new episode! Looking forward to 21.12!_

### Worth watching

#### Software progress
* Tobias Frisch: [GNUnet Messenger: December 2021](https://www.youtube.com/watch?v=BNKTwIu6Vdw). _This is looking promising, although I still need to read up on GNUnet._
* Avisando: [PinePhone Megapixels Test (end 2021)](https://www.youtube.com/watch?v=w_RMePIsArA).

#### PinePhone Usability
* Arbitrary Tech: [Pinephone Usability Review Q4 2021](https://www.youtube.com/watch?v=vPHbaJVg-8Q). _Plasma Mobile is getting there._
* Twinntech: [Pinephone vs Blackberry](https://www.youtube.com/watch?v=sEGzVzmB1eQ). _If that first take was too positive, this one is less positive. Also, confirmation bias and other fun phenomena at work!_

#### PinePhone Pro Unboxing and First Boot
* Niccolò Ve: [First Look At The Pinephone Successor!](https://tube.kockatoo.org/w/aVQTimWLJdP6K58KYXGLsr). _A fun look at Rockchips Android!_
* Iziac's Linux: [Taking a look at the PinePhone Pro](https://www.youtube.com/watch?v=9nHW--NheTI). _Bad camera, but well informed!_

#### Ubuntu Touch Plaza
* UBports: [Ubuntu Touch Q&A 114](https://www.youtube.com/watch?v=L49H55lnYUw). _The last Q&A of 2021: Featuring MMS improvements, magnetometer progress in 16.04 and more, all going into OTA 21 today; camera permissions following later. Focal gets meta-packages among other things, moving forward. Lomiri Debian packaging is moving forward as well. Also, new apps and some community questions._
* Crypto Contrarian Concierge: [Ubuntu touch smartphone Linux ubports in 2021](https://www.youtube.com/watch?v=tTRIXfxFxPs). _Um, yeah._

#### Virtual Conference Talks
* Yocto Project: [“Phosh: A GNOME based Wayland shell and compositor” by Tim Orling](https://www.youtube.com/watch?v=LAUPZhRPHAY). _Great to see Phosh on/in Yocto!_

#### JingPad corner
* Niccolò Ve: [JingPad Review: The KDE Plasma Fork iPad?](https://tube.kockatoo.org/w/9kgKnd8KuLnwnVCHYAwuC5)
* JingOS: [\[Dev News\] JingOS ARM 1.1.1 GUI Uninstall and JingPad Android App Support V0.7 Demo](https://www.youtube.com/watch?v=Y-Wi2jvVwqQ)

#### Shorts
* Siva Bharani & Friends: [Sailfish OS - OnePlus 6 = Good feel #Shorts #OnePlus6 #SailfishOS #OnePlus6in2022](https://www.youtube.com/watch?v=h0qb3l_xGlM).


### Missed your story?
If we missed an important bit of news, please get in touch. If you want to make sure we don't miss your app release, distro update, blog post or video, just ping us on social media or send me an email! _It's very much appreciated!_

### Stuff I did

#### Content
Nada.

#### Random
* I've started a discussion on LINMOBapps/LinuxPhoneApps called "[[Draft/Discussion] Future: Turning this list into LinuxPhoneApps.org](https://framagit.org/linmobapps/linmobapps.frama.io/-/issues/10)". _Please join the discussion, every idea is welcome!_
* I have installed [Fedora Silverblue on a ThinkPad Tablet 10 2nd](https://fosstodon.org/@linmob/107497364791471085). _Sometimes X86_64 is so much easier._

#### LINMOBapps and Linux Phone Apps
This week four apps were added:
* [Whale](https://invent.kde.org/carlschwan/whale), a very simple file explorer using Kirigami;
* [Hash-o-Matic](https://invent.kde.org/carlschwan/hash-o-matic), a simple hash validator allowing to compare two files, generate the checksum of a file and verify if a hash matches a file;
* [Kompass](https://invent.kde.org/smigaud/kompass), a compass application for plasma mobile, and
* [Wifi2QR](https://git.sr.ht/~wisperwind/wifi2qr), a tiny GTK utility to display QR codes to easily connect to any Wifi for which a NetworkManager connection profile exists.

There's plenty back log to still work through, but the momentum is promising. :D

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master) on LINMOBapps this week. And please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
