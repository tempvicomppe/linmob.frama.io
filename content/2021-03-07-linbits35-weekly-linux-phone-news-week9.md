+++
title = "LinBits 35: Weekly Linux Phone news / media roundup (week 9)"
aliases = ["2021/03/07/linbits35-weekly-linux-phone-news-week9.html", "linbits35"]
author = "Peter"
date = "2021-03-07T21:45:00Z"
layout = "post"
[taxonomies]
authors = ["peter"]
tags = ["PINE64", "PinePhone", "Purism", "Librem 5", "SailfishOS", "postmarketOS", "Mobian", "Phosh", "Plasma Mobile", "MauiKit", "LINMOBapps"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

New releases of Phosh and Portfolio, Element video chat on Fedora on PinePhone and more! _Commentary in italics._
<!-- more -->

### Software development and releases
* [Portfolio 0.9.10](https://blogs.gnome.org/tchx84/2021/03/01/portfolio-0-9-10/) has been announced. _This file manager gets better and better!_
* [Phosh 0.9.0](https://social.librem.one/@agx/105825512791153570) has been released.  It now supports GNOME's OSD DBus protocol, indicates microphone hardware kill switch state, read the full [release notes](https://source.puri.sm/Librem5/phosh/-/releases/v0.9.0) for more. 

### Worth noting
* @calebccff: [Quick video of Phosh running on the OnePlus 6 with Linux 5.11 and postmarketOS!](https://twitter.com/calebccff/status/1366145916978233345).
* Purism have made [some progress with hardware enablement](https://source.puri.sm/Librem5/linux-next/-/issues/44#note_144963) of the Librem 5 back camera.

### Worth reading 
* Plasma Mobile: [Plasma Mobile February Update](https://www.plasma-mobile.org/2021/03/01/plasma-mobile-update-february.html). _Impressive progress._
  * Linux Smartphones: [Plasma Mobile updates make the user interface more customizable (and a bit more Android-like)](https://linuxsmartphones.com/plasma-mobile-updates-make-the-user-interface-more-customizable-and-a-bit-more-android-like/). _Brad's take!_
* NXos.org: [Maui Apps 1.2.1 released, and more!](https://nxos.org/maui/maui-apps-1-2-1-released-and-more/). _I really need to check whether all of these apps are already listed on LINMOBapps._
* Purism: [Charging the Librem 5](https://puri.sm/posts/charging-the-librem-5/). _Note how the current numbers are somewhat spun into a success._
* Purism: [My First Week of Librem 5 Convergence](https://puri.sm/posts/my-first-week-of-librem-5-convergence/). _I have not used the Librem 5 in convergence mode a lot yet, but this does not match my experience. I mean, yes, you can definitely use it as a convergent device and there's a definite advantage to having one device and not having to sync data all the time, but 3GBs of RAM are just seriously limiting compared to a somewhat current laptop when it comes to web browsing alone. It's doable, but it definitely requires behavorial adjustments to make it work._
* Purism: [Librem 5 News Summary: February 2021](https://puri.sm/posts/librem-5-news-summary-february-2021/). _A small update without too much new information._
* Purism: [Disassemble Librem 5](https://puri.sm/posts/disassemble-librem-5/). _Nicely done!_
* Linux Smartphones: [Now you can make video calls on a PinePhone (but it’s very much a work in progress)](https://linuxsmartphones.com/now-you-can-make-video-calls-on-a-pinephone-but-its-very-much-a-work-in-progress/). _I doubt that running your own webserver is necessary. The new kernel, the [env variables in the launcher](https://marius.bloggt-in-braunschweig.de/2021/03/04/pinephone-breakthrou-on-fedora-pinephones/) are likely the key to getting this working. I'll spend some time on this next week._
* Spaetzblog: [Customizing the Librem5 ringtone (works for Mobian too)](https://sspaeth.de/2021/03/customizing-the-librem5-ringtone/). _If you want to change your ringtone on a Phosh distro, read this._


### Worth watching

* Kevin Veroneau: [Stock Mobian on PinePhone experience](https://www.youtube.com/watch?v=5XzYoWw8bTY). _A demo of wf-recorder recording the onboarding process on Mobian._
* Kevin Veroneau: [Running virt-manager on the PinePhone](https://www.youtube.com/watch?v=dQP9kj8t-ME). _The PinePhone as a sys admin tool. Nice!_
* iMakeFOSS: [Interview with @imakefoss curator Carl Schwan](https://www.youtube.com/watch?v=bdYqrJ_63oY). _Carl writes a lot of software for the PinePhone._

#### Unboxing corner
* AnotherKiwiGuy: [PinePhone Unboxing & First Impressions](https://media.kaitaia.life/videos/watch/2a047c0c-afbe-4880-b4f0-ba1c7d64ae3d). _Nice first impression video!_
  * AnotherKiwiGuy: [PinePhone Convergence Testing](https://media.kaitaia.life/videos/watch/bf83b6f2-e2ed-4375-a8da-80bddaad2a87). _A short follow-up video demoing convergence._
* Marcus Adams: [Pinephone Mobian Community Edition Unboxing](https://www.youtube.com/watch?v=SKAenoXe2gw).
* Niccolò Ve: [KDE Community Edition PinePhone Unboxing and First Try!](https://www.youtube.com/watch?v=AqtFyG4xUB0). _Nice unboxing! While you're at it, check out [Niccolo's Kirigami tutorial](https://www.youtube.com/watch?v=hvmmenRsmAo), too._
* Misc: [Purism Librem 5 Evergreen Unboxing in Deutsch #1](https://www.youtube.com/watch?v=y-uq6ss_IaE). _German Unboxing video on the Librem 5, that does a great job at explaining the project and includes some light disassembly. Use automatic translated subtitles._

#### Tutorial corner
* TechHut: [Distro-Hopping a Linux Phone! - How to Flash an OS on the PinePhone.](https://www.youtube.com/watch?v=Zf0zwq6jI30). _I guess I finally need to cut my old live stream so that we finally have a flashing howto showcasing GNOME Disks!_
* Privacy & Tech Tips: [Howto: SDR/GQRX On Pinetab/Pinephone, Listen To Voice Transmissions (Filtering, Modes, Gain/Squelch)](https://www.youtube.com/watch?v=IgqcJsZRdmw). _Great SDR how to!_
* Privacy & Tech Tips: [Howto: Open Pinetab Cover (Safely) + Adding SDR Antenna Port + LoRa Radio Tests (GQRX Linux)](https://www.youtube.com/watch?v=V-C6JzXhXbA). _Great PineTab and SDR how to!_
* Caleb Connolly: [Guide: How to build and install postmarketOS on your OnePlus 6/6T](https://www.youtube.com/watch?v=2dQaj3GdkfA). _Great tutorial by Caleb!_

### Stuff I did

#### Content

I published one video and one blog post this week. 
The video is called "Sailfish OS 4.0.1.48 on the PinePhone", and it's just about that.
Watch it on [DevTube](https://devtube.dev-wiki.de/videos/watch/b8810def-644e-4448-b24e-4d33d6a6fa9a), [Odysee](https://odysee.com/@linmob:3/sailfish-os-4.0.1.48-on-the-pinephone:9) or [YouTube](https://www.youtube.com/watch?v=DcdNwlmG1yk). 

Please make sure to carefully read the description, as this release of SailfishOS is not yet available as an image and you need to upgrade manually.

#### LINMOBapps

I also wrote a [blog post about the progress with LINMOBapps in January and February and about what's to come in the future](https://linmob.net/2021/03/07/linmobapps-additions-changes-january-february-2021-and-plans.html). 
That aside, I did not add any apps, just added something to the task list. I was very happy to see a [merge request](https://framagit.org/linmobapps/linmobapps.frama.io/-/merge_requests/5) that added a couple postmarketOS packages &mdash; thanks Alexander! [A little more maintenance happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). I also added some more [PKGBUILDs](https://framagit.org/linmobapps/linmobapps.frama.io/-/tree/master/PKGBUILDs). Please [do contribute](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/tasks.md)! 

 
