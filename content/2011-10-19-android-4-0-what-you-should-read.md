+++
title = "Android 4.0 - What you should read!"
aliases = ["2011/10/android-40-what-you-should-read.html"]
date = "2011-10-19T20:27:00Z"
[taxonomies]
tags = ["Android", "Android 4.0 Ice Cream Sandwich", "Galaxy Nexus", "google", "news", "platforms"]
categories = ["software", "shortform",]
authors = ["peter"]
+++

It's the evening, but I don't quite have the mind to write a lot regarding Android 4.0, it's hard to say anything before having used it, anyway.

So I am just sharing some links, which you should definitely check out.

* <a href="http://developer.android.com/sdk/android-4.0-highlights.html#UserFeatures">developer.android.com: Android 4.0 highlights for users</a>
* <a href="http://developer.android.com/sdk/android-4.0-highlights.html#DeveloperApis">developer.android.com: Android 4.0 highlights for developers</a>
* <a href="http://thisismynext.com/2011/10/18/exclusive-matias-duarte-ice-cream-sandwich-galaxy-nexus/">thisismynext.com: Exclusive: Matias Duarte on the philosophy of Android, and an in-depth look at Ice Cream Sandwich</a>

This not much, but the rest is essentially redundant information. Wait for my thoughts on the Galaxy Nexus and Android 4.0 tomorrow.
