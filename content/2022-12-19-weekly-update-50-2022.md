+++
title = "Weekly GNU-like Mobile Linux Update (50/2022): postmarketOS 22.12 and the PineTab2"
date = "2022-12-19T22:32:13Z"
updated = "2022-12-20T06:52:00Z"
draft = false
[taxonomies]
tags = ["postmarketOS","PINE64 Community Update","PineTab","PineTab2","SailfishOS","Manjaro","Ubuntu Touch",]
categories = ["weekly update"]
authors = ["Peter"]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
update_note = "Added TuxPhones article I missed due to collecting feeds a bit early."
+++

Sailfish OS approaching daily-drivability on PinePhone, gtk-file-chooser memes coming to an end and more.

<!-- more -->

_Commentary in italics._

### Hardware
PINE64 pre-announced the PineTab 2, powered by the Rockchip RK3566 known from the Quartz64, PineNote and other devices. To get it from the horses mouth, read the Community Update
- PINE64: [December Update: Merry Christmas and Happy New PineTab](https://www.pine64.org/2022/12/15/december-update-merry-christmas-and-happy-new-pinetab/)
As always, this got some coverage:
  - Ars Technica: [PineTab 2 is another try at a Linux-based tablet, without the 2020 supply crunch](https://arstechnica.com/gadgets/2022/12/pinetab-2-is-a-rockchip-based-linux-powered-repairable-tablet/)
  - CNX Software: [PineTab2 Linux tablet to feature Rockchip RK3566 SoC, up to 8GB RAM, 128GB eMMC flash](https://www.cnx-software.com/2022/12/16/pinetab2-linux-tablet-rockchip-rk3566-soc-up-to-8gb-ram-128gb-emmc-flash/) 
  - Liliputing: [Pine64's PineTab2 will be a Linux-friendly tablet with an RK3566 processor](https://liliputing.com/pine64s-pinetab2-will-be-a-linux-friendly-tablet-with-an-rk3566-processor/)
Our friends at TuxPhones compare it to other offerings you may also consider:
- TuxPhones.com: [The PineTab2 is a new, faster Linux tablet - and it's not alone](https://tuxphones.com/pinetab2-rk3586-linux-tablet-juno-tablet-fydetab-duo/)[^1]

Pricing and details (e.g., display resolution, the wireless chipset or whether it will have SPI for solid booting - on prototypes SPI is not populated) are not finalized yet, it will be out some time after Chinese New Year (January 21st to 27th, 2023) - so some time in February or March. _Personally, my enthusiasm is slightly tempered by the fact that PINE64 went with a custom mainboard for the device - I would loved to see them use the SoQuartz for this, in order to allow for future upgradability. Especially with all of PINE64's plans for RISC-V, it would have allowed for an easy road towards a RISC-y tablet._

If you're reading this and are not familiar with PINE64 or the RK3566, make sure to read Counter Pillow's excellent post now, and best before you pull the trigger in March, so that you know what you're getting into:
- CounterPillow: [RK3566 And PINE64, An Overview | A Laptop For My Pillow](https://fratti.ch/articles/posts/rk3566-and-pine64-an-overview/)

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#74 Decades Later](https://thisweek.gnome.org/posts/2022/12/twig-74/)
- Georges Stavracas: [Maintainership of GNOME Settings](https://feaneron.com/2022/12/15/maintainership-of-gnome-settings/)
- GTK Development Blog: [A grid for the file chooser](https://blog.gtk.org/2022/12/15/a-grid-for-the-file-chooser/)
  - Georges Stavracas: [the burial of the filechooser meme](https://feaneron.com/2022/12/14/the-burial-of-the-filechooser-meme/)

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: Wayland fractional scaling! Oh, and we also fixed multi-screen](https://pointieststick.com/2022/12/16/this-week-in-kde-wayland-fractional-scaling-oh-and-we-also-fixed-multi-screen/)
- Carl Schwan: [Tokodon 22.11.2 release](https://carlschwan.eu/2022/12/18/tokodon-22.11.2-release/). _I wonder why there's all of a sudden so much interest in Mastodon clients..._
- dot.kde.org: [Join Season of KDE 2023](https://dot.kde.org/2022/12/14/join-season-kde-2023)
- dot.kde.org: [Linux App Summit 2023 will be held in Brno](https://dot.kde.org/2022/12/13/linux-app-summit-2023-will-be-held-brno)
- Phoronix: [KDE Lands Wayland Fractional Scaling Support](https://www.phoronix.com/news/KDE-Lands-Wayland-Frac-Scaling)

#### Nemo Mobile
- neochapay [I think #nemomobile now ready to migrate to Qt6 https://github.com/nemomobile-ux/lipstick/pull/67](https://twitter.com/neochapay/status/1602374487974072320#m)
- While things are generally winding down a bit towards the end of the year, Nemo Mobile are doing the #adventofcode thing. [Make sure to follow Jozef Mlich on the Fediverse!](https://fosstodon.org/@jmlich)

#### Sailfish OS
- [Sailfish Community News, 15th December, Season's Greetings](https://forum.sailfishos.org/t/sailfish-community-news-15th-december-seasons-greetings/13781)
- [Adam Pigg on Twitter: ".@thepine64 #pinephone waking from deep sleep (see CPUs are halted) on incoming call."](https://twitter.com/adampigg/status/1603132663220797440)

#### Ubuntu Touch
- UBports News: [Ubuntu Touch Q&A 121](http://ubports.com/blog/ubports-news-1/post/ubuntu-touch-q-a-121-3871)

#### Distributions
- postmarketOS Blog: [v22.12: The One With Napali Calling](https://postmarketos.org/blog/2022/12/18/v22.12-release/)
  _Yes, stable may be boring, but less breakage is always nice - and postmarketOS do the best they can to make their stable releases actually stable. If you've been longing for performance, the improvements to call audio for Snapdragon 845 devices (SHIFT6mq, OnePlus 6(T), Xiaomi Poco F1) offer a whole new league of options for all those, that actually want to (or have to) do phone calls on their pocketable computers. Also: The Fairphone 4 is supported by 22.12, so it must be coming along!_ 
  - Lemmy - postmarketOS: [v22.12: The One With Napali Calling](https://lemmy.ml/post/662401)
- Breaking updates in pmOS edge: [libtiff related breakage in Alpine edge](https://postmarketos.org/edge/2022/12/16/libtiff/)
- Manjaro PinePhone Plasma Mobile: [Beta 14 RC 2](https://github.com/manjaro-pinephone/plasma-mobile/releases/tag/beta14-rc2)
- Manjaro PinePhone Plasma Mobile: [Beta 14 RC 1](https://github.com/manjaro-pinephone/plasma-mobile/releases/tag/beta14-rc1)

#### Stack
- [Guido Günther tagged version 0.0.2 of #feedbackd](https://social.librem.one/@agx/109518822668515937)

#### Linux 
- Phoronix: [Linux 6.2 Introduces Several More Touchscreen Drivers](https://www.phoronix.com/news/Linux-6.2-Input)

#### Matrix
- Matrix.org: [This Week in Matrix 2022-12-16](https://matrix.org/blog/2022/12/16/this-week-in-matrix-2022-12-16)

### Worth noting
- Emergency Contact Information is an important feature. [Chris (kop316) is working on bringing it to Phosh!](https://fosstodon.org/@kop316/109520726592801129)
- [NSG650 on Twitter: "This was painful but here it is. Ladybird running on postmarketOS aarch64! Touch support is ehh.](https://twitter.com/nsg650/status/1604036718742700032) _If you're into alternative web browsers, this is great news!_
- [Project Insanity: "Nice, looks like maui-shell is already running on #NixOS 🚀 Hopefully it can be merged soon"](https://fosstodon.org/@pi_crew@chaos.social/109528469094424015)
- [Guido Günther just keeps improving phosh-osk-stub!](https://social.librem.one/@agx/109513215258633453)

### Worth reading
- PINE64: [December Update: Merry Christmas and Happy New PineTab](https://www.pine64.org/2022/12/15/december-update-merry-christmas-and-happy-new-pinetab/). _Great read, make sure to also read the look back ... and can't await FOSDEM!_
  - [r/PINE64official comments](https://reddit.com/r/PINE64official/comments/zmpagc/december_update_merry_christmas_and_happy_new/)
- Purism: [Libadwaita in the Wild](https://puri.sm/posts/libadwaita-in-the-wild/)
- CNX Software: [Linux 6.1 LTS release - Main changes, Arm, RISC-V and MIPS architectures - CNX Software](https://www.cnx-software.com/2022/12/13/linux-6-1-lts-release-main-changes-arm-risc-v-and-mips-architectures/) _PinePhone Pro, everybody!_
- immychan: [PineBuds Pro Review](https://immychan.neocities.org/articles/tech/PineBudsProReview/PineBudsProReview.html)

### Worth watching
- PINE64: [December Update: Merry Christmas and Happy New PineTab](https://www.youtube.com/watch?v=WvG2jUS6n_s) _Yet another excellent video summary by PizzaLovingNerd!_
- Linux Lounge: [Pine64 Announced the PineTab 2!](https://www.youtube.com/watch?v=kJA41BAAV8g)
- CodingBite: [5 Best Linux SmartPhones to Watch Out in 2023](https://www.youtube.com/watch?v=yVQkmw2eWyI)
- InFerNo_: [Half Life on PinePhone](https://www.youtube.com/watch?v=bSx9B5nb_A0)
- nixgoat: [mi a3 boots postmarketOS by itself!](https://tube.nixgoat.me/w/b136878e-c4f0-4227-adde-32f4b4c33847)
- Ubuntu OnAir: [Ubuntu Summit 2022 | Ubuntu in your hands: The Going Ons at UBports](https://www.youtube.com/watch?v=d1sztciuuEw) _Great talk!_
- CodeInc: [What is UBUNTU TOUCH?](https://www.youtube.com/watch?v=boAqXdfFDug)
- 휴대전화 박물관 Phone Museum : [Ubuntu Touch Apps with Nexus 5](https://www.youtube.com/watch?v=duaJWWaEFEs)
- koshikawa.: [Redmi 9C Ubuntu Touch install moment (UBports)](https://www.youtube.com/watch?v=M3-pOBtj5oE)
- Continuum Gaming: [Microsoft Continuum Gaming E343: Fun App to create beats for Sailfish OS](https://www.youtube.com/watch?v=lDqTCNXiGhI)

### Thanks

Huge thanks again to Plata for [a nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A) for the next one! __If you just stumble on a thing, please put it in there too - all help is appreciated!__

PS: I'm looking for feedback - [what do you think?](mailto:weekly-update@linmob.net?subject=Feedback%20on%20Weekly%20Update)

[^1]: The article by TuxPhones.com was added after initial publication.
