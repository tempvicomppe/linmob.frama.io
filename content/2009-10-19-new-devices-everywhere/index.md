+++
title = "New devices everywhere"
aliases = ["2009/10/new-devices-everywhere.html"]
date = "2009-10-19T10:34:00Z"
[taxonomies]
tags = ["acer", "acer liquid", "Android", "donut", "eclair", "Motorola", "motorola droid", "omap3", "smartv5", "snapdragon",]
categories = ["hardware", "personal",]
authors = ["peter"]
+++
_Christmas holidays are no more that far away, and so many companys get their product portfolios reader for the holidays._
<!-- more -->
<a href="acerliquid.jpg"><img style="float:right;margin:0 0 10px 10px;cursor:pointer;cursor:hand;width: 240px;height: 320px" src="acerliquid.jpg" border="0" alt="Acer Liquid"  /></a>

But that wasn't what I wanted to write about. I rather want to comment on all these new mobile smartphonish devices, that are on their way to market right now&mdash;we see a new generation of devices&mdash;in terms of performance.

Being powered mostly by ARM Cortex A8 based SoCs, manufactured mainly by Texas Instruments (OMAP 3 series) and Qualcomm (Snapdragon) these devices actually have the power to do &#8220;real computing tasks&#8221; if you want to call it like that&mdash;running on the same chips as the still awaited &#8220;smartbooks&#8221;, which are basically netbooks powered by ARM CPUs.

This said, I just want to name a few devices running Linux based operating systems: <a href="http://www.boygeniusreport.com/2009/10/19/motorola-droid-hands-on/">Motorola Droid</a>, <a href="http://www.my-symbian.com/other/preview_n900.php">Nokia N900</a>, <a href="http://www.engadget.com/2009/10/16/acer-liquid-slips-through-human-fingers-but-not-before-taking-p/">Acer Liquid</a>&mdash;they all rely on ARM Cortex A8 technology and feature WVGA touchscreens.

While all these devices are really nice (e.g. I really like the Motorola Droid and hope that Motorola will offer something comparable for GSM networks (the Droid will be on Verizons CDMA networks) I actually do not consider to buy any of these.

&#8216;Why?&#8217;, you might ask, and well, the answer is that I am actually pretty sad about the manufacturers, that just make these devices as smartphones, not as mobile computers&mdash;even if some claim so. Still not clear what I want to say? 
It is about some simple features these devices lack, though they could actually hardware wise support it. #1 is USB OTG or USB host. Really, this would make these devices much more interesting, not for just for me, but e.g. for IT professionals as well&mdash;and #2 is a video output, e.g. HDMI or something similar. If the devices would feature these features, they would be real mobile computing devices if you ask me.

Of course there are reasons why these devices are limited. The manufacturers always would have to supply more drivers for external devices, or open their devices so much, that skilled people would be able to get themselves what they need. Than, considering Acer and Nokia are selling netbooks as well, such mobile computing devices would possibly be bad for netbook sales&mdash;though I don't really believe that- netbooks aren't pocketable and another cup of tea. And the last reason is, that there is a rather low demand for these features&mdash;only real enthusiasts request such strange stuff ;)

Now there is one more question one might ask: Is there a device which is pocketable and has the features mentionned some lines ago? Well, there is one&mdash;or at least will be one device that sounds pretty promising.

The SmartV5, which is basically the known cheapo-MID with more power inside (supposedly some Cortex A8 + 256MB Ram

__UPDATE:__ This might be wrong, as the <a href="http://www.jiongtang.com/blog/html/smartq5/smartv7-pictures-and-specification.html">SmartV7 MID</a> uses a Telechips TCC8900 chipset&mdash;I couldn't find any further datasheets or information on it, but it is believed that this is an ARM11 chipset with video acceleration (somewhat comparable to the nVidia Tegra) and a HDMI output&mdash;manufactured by the chinese company Smart Devices, it is some kind of open&mdash;as it will be available with three different operating systems, like the companys known offerings: Android, Ubuntu and Windows CE&mdash;and that's just what the manufactorer supplies.

But as there is always something to complain, it doesn't have 3G&mdash;you can connect your surfstick via USB OTG, but there is no other option.

Anyway, you would not use that device as a phone anyway, featuring a 4.x&#8221; display, it is not _that_ pocketable&mdash;but I still like it and look forward to some reviews of it&mdash;if the reviewers are not too disappointed about the build quality, i am likely to order one of these devices. 

If I have the money to do so&mdash;I need a new notebook, as my current one (HP Compaq nx6325) just turned three years old. But this an entirely different topic&#8230;
