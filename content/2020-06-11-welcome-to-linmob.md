+++
title = "Welcome to linmob.net (again)!"
aliases = ["2020/06/11/welcome-to-linmob.html"]
date = "2020-06-11T16:42:18Z"
layout = "post"
[taxonomies]
authors = ["peter"]
tags = ["first post", "PinePhone"]
categories = ["shortform", "internal"]
+++
For quite some time &mdash; since Purism announced their Librem 5 crowdfunder in 2017 &mdash; I have been thinking about bringing this blog back, that had been downgraded to a [category in my personal blog][category-link]. 
<!-- more -->
Now, that I got a [Pine64 PinePhone][pinephone] into my hands earlier this week, I finally got around to invest the time in restarting this blog. I haven't decided yet on whether I will bring the old content back or not and would appreciate feedback on that matter.

If you've found this blog and have specific questions, don't hesitate to contact me. I'll try to answer your questions as well as I can and I always appreciate constructive feedback.

[category-link]: https://brimborium.net/category/linmob
[pinephone]: https://www.pine64.org/pinephone/ 
