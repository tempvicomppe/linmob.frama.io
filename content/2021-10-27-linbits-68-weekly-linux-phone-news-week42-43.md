+++
title = "LinBits 68: Weekly Linux Phone news / media roundup (week 42/43)"
date = "2021-10-27T21:52:00Z"
draft = false
[taxonomies]
tags = ["PinePhone", "PinePhone Pro", "Librem 5", "Manjaro Phosh", "Nemo Mobile","levinboot","LINMOBapps","Matrix",]
categories = ["weekly update"]
authors = ["peter"]
+++

_Yet Another week in Linux Phones:_ 
Kernel progress for the Librem 5, UNISOC and old Tegra SoCs, Plasma Mobile Dev builds with Modem Manager; Nemo Mobile progress videos, Matrix news, new releases of Numberstation, phoc and DanctNIX and more! <!-- more --> _Commentary in italics._ 

### Software releases

* phoc, the compositor component of Phosh has seen another release, go read the [release notes](https://gitlab.gnome.org/World/Phosh/phoc/-/releases#v0.9.0). _Lots of bug fixes, and, I quote:_ This release changes the way application IDs are reported for GTK3 applications. If you're using scale-to-fit, you may need to adjust your configuration._
* Numberstation, Martijn Braam's excellent OTP client, [now allows to import and export data to make moving your OTP needs between devices much easier](https://fosstodon.org/@martijnbraam/107164520957162440).
* Manjaro have started [publishing Plasma Mobile ModemManager Dev builds](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases). _The fact that these are seperate builds from the usual dev builds should give you an idea about their level of maturity._
* DanctNIX Arch Linux ARM has seen [another release on October 22nd](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20211022). IMHO, the big new feature here is the new mesa-git variant, which seems to improve performance in my testing (no benchmarks, just judging from my in use experience.

### Worth reading

#### Good Kernel News
* TuxPhones: [More support for UNISOC and Spreadtrum chips lands in the Linux kernel](https://tuxphones.com/unisoc-spreadtrum-tanggula-tiger-soc-mainline-linux-kernel-support/). _While it's too early to say how big this effort is and how long it's going to continue, it's good to see some progress here._
* Purism: [Purism and Linux 5.14 and Linux 5.15](https://puri.sm/posts/purism-and-linux-5-14-and-linux-5-15/). _Truly great progress. Also, from what I saw from just having a glance at source.puri.sm, libcamera might land rather soon._ 
* Phoronix: [Linux Continues To Improve Power Management For Older NVIDIA Tegra SoCs To Avoid Overheating](https://www.phoronix.com/scan.php?page=news_item&px=Linux-5.17-Tegra-PM-Hot)
* xnux.eu log: [Pinephone HDMI hot-plug-detection HW bug fixed](https://xnux.eu/log/#050). _I bet a lot of people are now asking "5.15 wen?" in distro chat rooms._

#### PinePhone Pro multi boot
* xnux.eu log: [Pinephone Pro – levinboot payload selection](https://xnux.eu/log/#049). _Multi boot landing - and being helpful for further kernel development!_

#### Userland Software progress
* This Week in GNOME: [#15 Sepia and App Updates](https://thisweek.gnome.org/posts/2021/10/twig-15/). _Great to read about Fractal's progress!_ 
* Carl Schwan: [This week in NeoChat](https://carlschwan.eu/2021/10/22/this-week-in-neochat/).
* Claudio Cambra: [Some big usability improvements and fixes, fixes, fixes — Kalendar devlog 20](https://claudiocambra.com/2021/10/24/some-big-usability-improvements-and-fixes-fixes-fixes-kalendar-devlog-20/). _Beta soon!_
* Carl Schwan: [Spellchecking with QML](https://carlschwan.eu/2021/10/25/spellchecking-with-qml/)
* Phoronix: [PipeWire 0.3.39 Brings Libcamera Plugin Improvements, Better Compatibility For JACK Apps](https://www.phoronix.com/scan.php?page=news_item&px=PipeWire-0.3.39-Released).
* Phoronix: [Alternative Python Implementation "Pyston" Plans For Greater Performance, 64-bit ARM](https://www.phoronix.com/scan.php?page=news_item&px=Pyston-Roadmap-2021). _Just including it as it might help with Python app performance._


#### App recommendations
* Gamey: [PINEPHONE: Matrix clients on Linux Phones!](https://gamey.tech/posts/pinephone-matrix-clients-on-linux-phones/). _Great post by Gamey! BTW, if you want to use Hydrogen as a web app on Phosh, just install it in GNOME Web (Epiphany) as a web app. While that's generally not super fast, performance is good enough with Hydrogen._

#### Cool features
* Liliputing: [Virtual mouse app for Linux phones makes desktop apps easier to use](https://liliputing.com/2021/10/virtual-mouse-app-for-linux-phones-makes-desktop-apps-easier-to-use.html). _Great post about an impressive [project](https://gitlab.com/CalcProgrammer1/TouchpadEmulator), make sure to watch the video (also linked below)!_


#### PinePhone Pro Posts
* BoilingSteam: [Pinephone Pro: Finally, A Tangible Alternative](https://boilingsteam.com/pinephone-pro-finally-a-tangible-alternative/). 

#### Making of the PinePhone Pro introduction video
* Vincent the Cat on PINE64: [How „Meet the PinePhone Pro“ was made](https://www.pine64.org/2021/10/27/how-meet-the-pinephone-pro-was-made/). _Great read!_

#### Now for something completely different: VR and Matrix
* Liliputing: [Unlocked Oculus Go software released, making the VR headset hacker-friendly](https://liliputing.com/2021/10/unlocked-oculus-go-software-released-making-the-vr-headset-hacker-friendly.html). _I am really looking forward to what people are going to come up with!_
* element blog: [Element One - all of Matrix, WhatsApp, Signal and Telegram in one place](https://element.io/blog/element-one-all-of-matrix-whatsapp-signal-and-telegram-in-one-place/). _Make sure to not get carried away by enthusiasm and read through the current caveats of this, but it's still great news for Linux Phones if you ask me! See also: [Beeper](https://www.beeper.com/)._

### Worth watching

#### Real PinePhone Pro videos
* Martijn Braam: [Phosh on the PinePhone Pro](https://spacepub.space/w/93pFi6dHKKnSp6XeATws6v). _Looking good!_


#### Other PinePhone Pro videos
* The Linux Experiment: [PinePhone Pro - Do Linux Phones have their flagship?](https://tilvids.com/w/5918a095-e8ad-4783-8609-5ec592e53d4b). 
* Mental Outlaw: [The PinePhone Pro - Finally a Linux Flagship Smartphone](https://www.youtube.com/watch?v=WA0rxLniBbc). _Bezel-less design.. for a future PinePhone, yeah, sure._

#### Nemo Mobile Progress
* Jozef Mlich: [Booting NemoMobile on PinePhone](https://www.youtube.com/watch?v=-rY6YQFGFdw). _Nice animation!_
* Сергей Чуплыгин: [Nemomobile device lock work](https://www.youtube.com/shorts/cSq9bZtt6_c). _Useful!_
* Сергей Чуплыгин: [Glacier UX: mouse support](https://www.youtube.com/shorts/dgALx3D3Ngk). 

#### Using Desktop Software on Phones
* CalcProgrammer1: [Touchpad Emulator Update - Automatic Rotation, Keyboard Toggle, Demo with External Monitor](https://www.youtube.com/watch?v=cXKvbc516gQ). _This is really impressive, especially that touchpad feature!_

#### Software Defined Radio experiments
* cemaxecuter: [DragonPhone w/ Headless Suscli Server + KerberosSDR w/ Wireless SigDigger Client Setup (PinePhone)](https://www.youtube.com/watch?v=Mlf4eny3hmE).

#### PinePhone for remote working?
* richieeee72: [Hybrid working using PinePhone, dock and VNC Connect](https://www.youtube.com/watch?v=kWaV54-htzU). _This is quite interesting, and it works quite okay!_

#### Sxmo Talk
* Tech Over Tea: [Let's Run DWM on A Pinephone | Nephitejnf](https://www.youtube.com/watch?v=scIyIKQ2yWw).

#### Matrix news
* Matrixdotorg: [Matrix Live S06E45 — FractalNext](https://www.youtube.com/watch?v=6Az0E2YG3z0). _If you care about Matrix, this is one to watch!_

#### PinePhone Reviews
* My Linux Diary: [Pinephone Review after a month and a half](https://www.youtube.com/watch?v=rGyRmMYQB3Q).

#### PinePhone "Review" "Reviews"
* Twinntech: [Pinephone honest review #2](https://www.youtube.com/watch?v=ANzUSB20Rj4). _Sorry everybody for causing this ... video by [comment](https://discord.com/channels/@me/792812923755167744/902980020719464489) (please take note of the [hashtag](https://en.wikipedia.org/wiki/Confirmation_bias))._

#### Ubuntu corner
* UBports: [Ubuntu Touch Q&A 111](https://www.youtube.com/watch?v=gVyCJPW98No). _Another great Q&A, with Dalton, Alfred and Florian. They discuss new apps, OTA 20, Ayatana indicators landing in 20.04, and lots of interesting community questions!_ 
* Neil Patel: [Ubuntu Touch for OnePlus 5T Initial Thoughts \[Blog\]](https://www.youtube.com/watch?v=z61sbMGOWMI).

#### Tablet fun
* Niccolò Ve: [Challenge: Use KDE Plasma ONLY With Touchscreen!](https://tube.kockatoo.org/w/76xwD2mHFm7CF8j11cZcMr). _While it actually seems challenging, I feel that a video like this one might help moving things forward._

#### Shorts
* cemaxecuter: [DragonPhone M17 Decoded by SDR++ on the PinePhone w/ RTLSDR](https://www.youtube.com/watch?v=FBINTHt_Zjg).
* JingOS: [Kodi works perfect on JingPad A1 and JingOS ARM!](https://www.youtube.com/watch?v=eZlfemVBGxA).
* CCF_100: [🤔 (Neverball on PinePhone)](https://www.youtube.com/watch?v=STlIPYA0pV4). 

### Stuff I did

#### Content
None :(

#### Random

* I tried to get [into Memes](https://fosstodon.org/@linmob/107157271745909095).
* I was kindly invited to join another podcast for one episode. More on this once the episode is out!

#### LINMOBapps

Activity has continued on the low level it reached last week. I've added one app:
* [Phosh Antispam](https://gitlab.com/kop316/phosh-antispam),  a program that monitors Gnome calls and automatically hangs up depending on the user's preferences. _Extremely useful if spam callers bug you. Once more great work by kop316!_

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master) on LINMOBapps this week. And please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)

#### Linux Phone Apps

Nothing, sadly.
