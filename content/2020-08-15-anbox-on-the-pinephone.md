+++
title = "Anbox on Mobian on the PinePhone (Android on PP #0)"
aliases = ["2020/08/15/anbox-on-the-pinephone.html"]
author = "Peter"
date = "2020-08-15T13:55:00Z"
layout = "post"
[taxonomies]
tags = ["PinePhone", "Video content", "Mobian", "Anbox", "Android 7", "Scaling", "Phosh", "Sqeekboard", "F-Droid", "Google Play Services", "microG"]
categories = ["howto"]
authors = ["peter"]

+++
By [public](https://twitter.com/linmobblog/status/1293075978651078661) [demand](https://fosstodon.org/web/statuses/104670894757918964) I finally made a [video about Anbox on the Pinephone](https://www.youtube.com/watch?v=v06KUrfs69k), which is one of the ways have Android apps on the Pinephone.

<!-- more -->

### Introduction
I had tried [Anbox](https://anbox.io/) before, way back when it was really popular (around ~2017). It also is really easy to install on [postmarketOS](https://postmarketos.org), where it is a `sudo apk add postmarketos-anbox` away, and I had failed to build it from the AUR ([Danct12 has pounced on the issue since](https://twitter.com/RealDanct12/status/1294309229516304390#m)). Therefore I followed [two](https://forum.pine64.org/showthread.php?tid=9815&pid=74676#pid74676) [posts](https://forum.pine64.org/showthread.php?tid=9815&pid=65378#pid65378) on the pine64.org forums, which explain the process of installing Anbox on [Mobian](https://www.mobian-project.org/) step by step. It's not easy, but at least to me, it seems that it runs faster on Mobian than it does on postmarketOS.

### Setting it up

_These instructions are likely outdated. Please follow the more current [instructions to install Anbox on Mobian on the Mobian Wiki](https://wiki.mobian-project.org/doku.php?id=anbox)._[^1]

The following is a bland copy of the instructions on the Pine64 forum, with a handful changes, along to the [Mobian Gitlab issue](https://gitlab.com/mobian1/issues/-/issues/63). Therefore all credit goes to _nas_, _jed_, and _Djhg2000_.

#### Download the image to your PinePhone

`wget http://anbox.postmarketos.org/android-7.1.2_r39-anbox_arm64-userdebug.img` [^2] 


#### Append contrib to the default SourcesList

> `printf '%s' ' contrib' | sudo tee -a /etc/apt/sources.list` 

This did not work for me. Just open `/etc/apt/sources.list` in Vim or an editor of your choice as root and type a space and contrib after the word main, so that it looks like this afterwards:

`deb http://deb.debian.org/debian bullseye main contrib`

#### Update and install the requisite packages

`sudo apt update && sudo apt -y install anbox adb`

#### Copy the Android image

`sudo mv android-7.1.2_r39-anbox_arm64-userdebug.img /var/lib/anbox/android.img`

#### Create a new service file

Using your favourite editor, create the file `/etc/systemd/system/dev-binderfs.mount`:

~~~~~
[Unit]
Description=binderfs

[Mount]
What=binder
Where=/dev/binderfs
Type=binder

[Install]
WantedBy=anbox-container-manager.service
~~~~~

#### Edit the anbox-container-manager.service unit file

This fix originates from [Mobian Issue #63](https://gitlab.com/mobian1/issues/-/issues/63).

Change the file `/lib/systemd/system/anbox-container-manager.service` (lines 4, 6, 10, 11)  so that it has the following content:

~~~~~~~
[Unit]
Description=Anbox Container Manager
Documentation=man:anbox(1)
After=network.target dev-binderfs.mount
Wants=network.target
Requires=dev-binderfs.mount
ConditionPathExists=/var/lib/anbox/android.img

[Service]
#ExecStartPre=/sbin/modprobe ashmem_linux
#ExecStartPre=/sbin/modprobe binder_linux 
ExecStartPre=/usr/share/anbox/anbox-bridge.sh start
ExecStart=/usr/bin/anbox container-manager --daemon --privileged --data-path=/var/lib/anbox
ExecStopPost=/usr/share/anbox/anbox-bridge.sh stop

[Install]
WantedBy=multi-user.target
~~~~~~~

#### Change the anbox-session-manager.service unit file

Change the file `/lib/systemd/user/anbox-session-manager.service` so that it has the following content:

~~~~~~
[Unit]
Description=Anbox Session Manager
Documentation=man:anbox(1)
After=graphical.target
Wants=graphical.target

[Service]
ExecStart=/usr/bin/anbox session-manager

[Install]
WantedBy=default.target
~~~~~~

#### Reload the service files

`sudo systemctl daemon-reload`

`sudo systemctl restart anbox-container-manager.service`

`systemctl --user daemon-reload`

`systemctl --user restart anbox-session-manager.service`

#### Try it

Anbox is now installed.
Anbox must be open to allow installation of APKs via adb, so start it.

`anbox launch --package=org.anbox.appmgr --component=org.anbox.appmgr.AppViewActivity`

A good first app to install is FDroid, the open-source Android store:

Download it:
`wget https://f-droid.org/F-Droid.apk`
and install it:

`adb install F-Droid.apk`

Android applications can be launched from the Phosh launcher (as .desktop files), or from the Anbox Application Manager.

#### Issues 
##### Not enough RAM
The 2GB RAM of the PinePhone are not that much, and Anbox is quite the RAM eater. Therefore, I use ZRAM with a really shitty script called `.zram.sh`, which has to be run as root:

~~~~~
modprobe zram
echo 3.5G > /sys/block/zram0/disksize
mkswap --label zram0 /dev/zram0
swapon --priority 100 /dev/zram0
~~~~~

I really should do this via systemd, but did not get around to it yet.
The [Mobian Wiki has more sane instructions](https://wiki.mobian-project.org/doku.php?id=tweaks#enable-zram) for that.

##### Internet Connection
While Anbox should work fine now even after reboots (as long as it has enough RAM), the network connection has to be set up manually every time, after a crash or a reboot:

~~~~~
sudo /usr/share/anbox/anbox-shell.sh
su
ip route add default dev eth0 via 192.168.250.1
ip rule add pref 32766 table main
ip rule add pref 32767 table local
~~~~~

I already tried to simplify this step via `adb`, but after `adb root` adb does not find the Anbox emulator anymore.

__EDIT, 22:50h CEST:__ _Djhg2000 [added their findings](https://github.com/anbox/anbox/issues/443#issuecomment-674418196) to a long standing issue at the Anbox project._

##### Keyboard

As shown in my video, the Phosh keyboard, Squeekboard, does not work with Android. Apparently, [this is a known bug with other SDL2 and Xwayland apps, too](https://source.puri.sm/Librem5/squeekboard/-/issues/218). My temporary fix was to just install and enable (you will need to do so in the Android settings) an Android software keyboard. I tried multiple and had the best results with [Simple Keyboard](https://f-droid.org/en/packages/rkr.simplekeyboard.inputmethod/), which has an option to scale it's height &mdash; I settled on 125 %. Now, don't think that this is a perfect solution &mdash; it most definitely is not, as you can't see the keyboard, it shows as a black or white (and thus on a white background sometimes invisible) rectangle. _You will not see the keys, you will need to guess their positions!_ But hey, it is working for text input.[^1] Another work around is, apparently according to the issue linked above, a hardware keyboard, e.g. a Bluetooth one. One more reason I can't wait for the [Pine64 hardware keyboard for PinePhone](https://www.pine64.org/2020/07/29/invitation-to-play-along/)! 

__EDIT, 21:45h CEST:__ _Replacing the Squeekboard binary with [virtboard](https://source.puri.sm/Librem5/virtboard) works, too ([tweet](https://twitter.com/linmobblog/status/1294717195566682114))._

##### Scaling

Apps look _blurry_ - they apparently don't if you set Phosh to 1x scaling. That however comes with an entirely different set of issues, e.g. a miniscule virtual keyboard, which you would than have to adjust. I think I could deal with the blurryness, but it would be great to see that issue fixed.
Secondarily, apps always scale to the full height of the device. This in unfortunate, as Phosh has bars with UI elements on the top and bottom of the screen which then obscure your Android apps. I haven't seen anything on how to even attempt fixing this problem, and will update this post if I do.

__Added October 7th, 2020:__ You can temporarily (until the next restart) the Phosh UI element overlay issue by running `adb shell wm overscan 0,35,0,35` ([tweet](https://twitter.com/linmobblog/status/1312696526859370496#m)).

##### Android services

Obviously, Anbox can only be a stop-gap solution to be used when there is no native or web-based alternative. Many of these apps may not work. One problem is that currently Android 7.1.2 (Nougat) is the most advanced release working with Anbox. Meanwhile, Google is working to get Android 11 ready. And: With the default image, you don't have any of Googles Play services, that many proprietary apps require (in case you are wondering, the [Aurora Store app](https://f-droid.org/en/packages/com.aurora.store/) can tell you, whether a certain app you desire requires these services). MicroG, an open-source reimplementation of some of the Play Services, [apparently works with Anbox](https://forum.pine64.org/showthread.php?tid=9815&pid=65378#pid65378). Also, there are images for amd64-PCs with included Play Services [available in the Arch User Repository], but I don't know whether these still work &mdash; and assuiming they do work, Google could stop this any day.

### Conclusion

The current state of Anbox can be summarized with 'it sort of works'. Unfortunately, the 2GBs of RAM the PinePhone ships with, are a limiting factor here, and everybody who pre-ordered the Convergence Package can be extra happy, as that extra Gigabyte of RAM will come in handy here, too. While a few bugs that affect the operation of Anbox are likely to be fixed within a couple of weeks, others may take way longer or forever. 
Luckily, there are other options to 'running' Android apps on the PinePhone, and I look forward to investigate and writing about these soon.

_In case you read up to this point, and did not watch my video yet, now is a good time to head over to [YouTube](https://www.youtube.com/watch?v=v06KUrfs69k) or an [invidious-instance](https://invidious.ad13.de/watch?v=v06KUrfs69k)._ 

__Added on August 16th, 2020:__ 

### Other distributions [^3]

While the above describes how I did enable Anbox on Mobian, the instructions should apply to other distributions that use systemd, too. You will need to obtain an Anbox package though, which might be tough.

Getting Anbox to work should be particularily easy on ArchLinux ARM/Huong Tram Linux, as _Danct12_ packaged Anbox for it. So become root, `pacman -S anbox anbox-image`, reboot and it should work just fine with a working keyboard and FDroid preinstalled. 

Manjaro offers their Phosh images with Anbox (including the Android image) preinstalled [for download](https://osdn.net/projects/manjaro-arm/storage/pinephone/phosh/).

I have made further videos on Anbox and published them on YouTube:

* [PinePhone: Installing Anbox on Arch Linux ARM, trying WhatsApp and Signal](https://www.youtube.com/watch?v=Y-9Wmki7DsU), _published September 6th, 2020._ 
* [Manjaro Phosh Alpha 4: Anbox Community Service (Anbox on PinePhone III)](https://www.youtube.com/watch?v=8Sha3R4PKSs), _published September 29th, 2020._ 

[^1]: This remark was added on December 25th, 2020.
[^2]: _October 7th, 2020_: You might want to look at the [PKGBUILD for Arch Linux ARM](https://github.com/dreemurrs-embedded/Pine64-Arch/blob/master/PKGBUILDS/pine64/anbox-image/PKGBUILD) to patch the image to make it better. Networking, keyboard and so on work on Arch Linux ARM.g
[^3]: This section was added on August 16th, 2020 and last updated (and rewritten) on October 7th, 2020.
