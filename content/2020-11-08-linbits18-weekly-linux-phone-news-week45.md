+++
title = "LinBits 18: Weekly Linux Phone news / media roundup (week 45)"
aliases = ["2020/11/08/linbits18-weekly-linux-phone-news-week45.html", "linbits18"]
author = "Peter"
date = "2020-11-08T12:30:00Z"
layout = "post"
[taxonomies]
tags = ["Librem 5", "Pine64", "PinePhone", "Maemo Leste", "postmarketOS", "Mutt", "Phosh", "Calendar", "Linux", "Ubuntu Touch", "Precursor", "p-boot", "LINMOBapps", "SailfishOS", "Podcasts"]
categories = ["weekly update"]
authors = ["peter"]
+++

_It's sunday. Now what happened since last LinBits?_

A lot: UBports Ubuntu Touch OTA-14, postmarketOS news, Librem 5 and Pyra shipping announcements and more. _Commentary in italics._
<!-- more -->

### Software releases and improvements
* [phosh 0.5.1](https://social.librem.one/@agx/105151150124355697), bringing bug fixes, translation updates and docked mode for tablets. Read the [full release notes](https://source.puri.sm/Librem5/phosh/-/releases) for more.
* [Mutt 2.0](http://www.mutt.org/relnotes/2.0/). _Now you may wonder why this in here, but since it works on the terminal of Linux phones too and it’s a number before the dot release I wanted to shine a light on this long running project._
* [Mæp](https://github.com/maemo-leste-extras/maep), a new maps app for Maemo Leste [has surfaced](https://twitter.com/maemoleste/status/1324158043899256833#m).
* [Ubuntu Touch OTA 14](https://ubports.com/blog/ubport-blogs-news-1/post/ubuntu-touch-ota-14-release-3728). _OTA 13 happened not too long ago, so this is a relatively minor one, fixing a few bugs and enabling more releases._

### Hardware announcements

* PINE64 replacement PinePhone mainboards with 3GBs of RAM are now [available for order](https://pine64.com/product/pinephone-community-edition-3gb-32gb-mainboard/?v=0446c16e2e66) at a [discount for UBports CE and Braveheart owners](https://pine64.com/product/pinephone-community-edition-3gb-32gb-mainboard-special-offer-for-braveheart-and-ubports-owners/?v=0446c16e2e66). _This is great news for all of us who ordered early, and want more RAM and less hardware bugs on Braveheart and UBports CE PinePhones without buying a whole new phone._

### Worth reading

* xnux.eu/Megi: [New p-boot released, new pre-built/updated kernels](https://xnux.eu/log/#024). _I really need to try this._
* Gamey: [Best Calendar app for the Pinephone & Librem5 // Calindori on ArchLinux Phosh](https://lbry.tv/@gamey:c/Best-Pinephone-Calendar:2). _He's 100% on point. Even if there was another Calendar app available (Maemo Leste’s qalendar is very much only for Maemo Leste), Calindori would still be a strong contender. I only which there was a way to patch Plasma’s and Gnome’s respective backend’s together to have a method to easily sync Calindori to services, e.g. Nextcloud._
* Amos B. Batto: [The questions that mobile phone reviewers never ask](https://amosbbatto.wordpress.com/2020/11/02/questions-mobile-phone-reviewers-never-ask/). _What he points out here is more than true. However, there are just to many phones being released to do this long term work._
* Linux Kamarada: [PinePhone, the Linux-based smartphone: everything you need to know about it](https://kamarada.github.io/en/2020/11/02/pinephone-the-linux-based-smartphone-everything-you-need-to-know-about-it/). _A great overview. I should really get to work on creating something similar._ 
* TuxPhones: [Precursor is a RISC-V, FPGA-assisted open hardware PDA aimed at developers and hackers](https://tuxphones.com/bunnie-studios-precursor-open-hardware-risc-v-linux-pda-phone/).  _Make sure to watch the talk by the guy who designed this wonderfully opinionated piece of hardware._
* TuxPhones: [Building responsive Linux mobile apps with libhandy 1.0 and Gtk3](https://tuxphones.com/tutorial-developing-responsive-linux-smartphone-apps-gnome-builder-gtk-libhandy-gtk-part-1/). _Raffaele upgraded his tutorial now that libhandy has stabilized. If you end up developing an app, please ping me so I can add it to LINMOBapps._
* LinuxSmartphones: [PinePhone 3GB/32GB upgrades are now available for purchase](https://linuxsmartphones.com/pinephone-3gb-32gb-upgrades-are-now-available-for-purchase/). _See above._
* Purism: [The Librem 5 Mass Production Shipping FAQ](https://puri.sm/posts/the-librem-5-mass-production-shipping-faq/). _Finally, Purism has put out a timeline. I will keep you all posted once I receive an email (you are going to notice the day I receive it as I will immediately publish an unboxing). That said, I can't really decipher the writing here to know for sure whether I will receive mine this year, so ... I am just staying positive._
* Liliputing: [DragonBox Pyra handheld Linux gaming PC begins shipping this month](https://liliputing.com/2020/11/dragonbox-pyra-handheld-linux-gaming-pc-begins-shipping-this-month.html). _Finally! I had to [joke on twitter](https://twitter.com/linmobblog/status/1324809371235323904#m) about this._


### Worth watching

*  Martijn Braam: [Replacing the main PCB in a PinePhone](https://www.youtube.com/watch?v=5GbMoZ_zuZs). _Just in time to PINE64 selling replacement PinePhone mainboards._
* HomebrewXS: [WhatsApp on the PinePhone](https://www.youtube.com/watch?v=rRRUEMOVPSQ). _If you need that app, you are going to be happy with these instructions._
* HomebrewXS: [Anbox on the PinePhone](https://www.youtube.com/watch?v=ahitAWmrqzU). _Anbox!_
* HomebrewXS: [homebrewOS Design](https://www.youtube.com/watch?v=mgN_sDHTVEU). _A new mobile OS project with a design focus. Check it out and maybe help them with development!_
* EF - Tech Made Simple: [Unboxing The PinePhone - Manjaro!](https://www.youtube.com/watch?v=Ci_CW9mp9CY). _The first Manjaro unboxing. If you are receiving a Manjaro PinePhone: 123456 is the PIN._
* Purism: [Librem5 Laptop mode](https://puri.sm/posts/librem-5-laptop-mode/). _This is a shorter, more concise version of my [PinePhone Convergence with Phosh video](https://devtube.dev-wiki.de/videos/watch/25d2e06b-ff3f-4ffa-a8cf-56f034e37199), only featuring a Librem 5 and a Nex Dock Touch instead of a PinePhone and a HP Elite X3 Lap Dock._
* Privacy & Security Tips: [Take A Look At PostmarketOS KDE Plasma Mobile: + Howto: Build/Install w/Pinephone MicroSdcard](https://www.youtube.com/watch?v=Y0pSu554kB8). _Great video detailing pmbootstrap._
* Privacy & Security Tips: [Mobian Pinephone: Axolotl- Signal Messenger Clone Available Now](https://www.youtube.com/watch?v=3cFb9Lq0ti0). _Come for the video, stay for the instructions. I failed on my attempts to get it running on Arch Linux ARM, but I am going to try again._
* Avisando: [PinePhone - Updating multi-boot image](https://www.youtube.com/watch?v=sPmD5vxyc6o). _This looks like it's just the p-boot upgrade._
* Gadgets Gadder: [PinePhone - Linux in Your Pocket All Details)](https://www.youtube.com/watch?v=C4nBXyv1CWc). _If you don't know, now you know ;-)._
* FOSSnorth: [Precursor - Bunnie Huang](https://conf.tube/videos/watch/33a60789-c678-458b-83b6-d321bf64778e). _If you‘ve been wondering why Precursor was designed the way it has been designed, why the FPGA, why the black and white screen, this talk answers it in depths. Must watch!_
* Prawnboy SG: [Sailfish OS on the pinephone](https://www.youtube.com/watch?v=Rh7SoJoZT_0). _A simple, silent video demoing Sailfish OS. Great to see more people publishing PinePhone content._
* SungHyun Kim: [P-BOOT Maemo Leste Boot](https://www.youtube.com/watch?v=sZL0bAOM1r8), [P-BOOT UBports Boot](https://www.youtube.com/watch?v=I2qwd4ZBhHo), [P-BOOT LuneOS Boot](https://www.youtube.com/watch?v=udPeT6SYS5c), [P-BOOT PureOS Boot](https://www.youtube.com/watch?v=eDKHSw5MHA4), [P-BOOT Mobian Boot](https://www.youtube.com/watch?v=45HyEWGt9NM), [P-BOOT KDE Neon Boot](https://www.youtube.com/watch?v=b42mX8XXzzA), [P-BOOT Pure OS Boot](https://www.youtube.com/watch?v=rT-uCyJgEhU) and [P-BOOT Sailfish Boot](https://www.youtube.com/watch?v=4fjGQJCBW-4). _P-BOOT. A lot of P-BOOT. Time for a few words of advice regarding the [P-BOOT multi boot image](https://xnux.eu/p-boot-demo/): Try it out, but don't use it as a daily/main distribution to play with it. With a different kernel, you'll run into different bugs and distributions won't be able to help you. Also: It's getting old._
* UBports: [Ubuntu Touch Q&A 88](https://www.youtube.com/watch?v=a-Z1_KKWL00). _Hardware announcements, the OTA 14 release [11:20] with delivering a stable channel for the Volla Phone, OTA 15 is likely going to deliver the Qt 5.12 rebasing [12:48]. UBports installer news, and more. Also, stay on for the actual Q&A [23:00]. Looks like they are onboarding someone for Anbox! Yay!_

### Stuff I did

#### Content
* I made a too long, slightly confused video about Convergence with Phosh. Watch it on [PeerTube](https://devtube.dev-wiki.de/videos/watch/25d2e06b-ff3f-4ffa-a8cf-56f034e37199), [LBRY](https://lbry.tv/@linmob:3/pinephone-convergence-with-phosh-and:f) or [YouTube](https://www.youtube.com/watch?v=y9pOCXy0Q94).
* Also, I made a shorter video on postmarketOS with Phosh running on the bq Aquaris X5. I am going to try and follow up this with a blog post. Watch the video on [PeerTube](https://devtube.dev-wiki.de/videos/watch/59bafeef-9942-4c40-ac39-390995982e5a), [LBRY](https://lbry.tv/@linmob:3/postmarketos-with-phosh-on-the-bq:c) or [YouTube](https://www.youtube.com/watch?v=ZZR3yAamwXA).

#### Podcasts
* If you can understand the german language, check out the [4th episode of GLN Podcast](https://gnulinux.ch/gln004-podcast), where I shared my opinion on the PinePhone starting at 1:01:40.
* Also, you can hear my voice around 1:01:56 on [Jupiter Broadcastings' Linux Unplugged Podcast, Episode 378](https://linuxunplugged.com/378).

#### LINMOBapps
Not much happened here, [just a few additions and some maintenance](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). 

