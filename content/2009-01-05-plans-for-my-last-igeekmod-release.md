+++
title = "Plans for my last igeekmod release"
aliases = ["2009/01/05/plans-for-my-last-igeekmod-release"]
author = "peter"
date = "2009-01-05T15:53:00Z"
layout = "post"
[taxonomies]
tags = ["A910", "A910i", "Android", "development", "i-geekmod"]
categories = ["projects", "personal",]
authors = ["peter"]
+++
<p>Well, as I don't look at my statistics, I don't know how many people are reading this blog. I know that it isn't that hard to find it using popular search engines, but that's all. Years ago, I cared about all this SEO stuff, which I am just bored of today&mdash;when I comment on engadget I don't link my blog, and so I do in many forums. 

This means, that sometimes the posts I write are mostly memorandums to myself. 
But this one isn't, it is some kind of announcement. I feel ready with my feature list and this means, that I'll  submit it here.

First of all, thanks to our chinese friends, we have EDGE on igeekmod R2&mdash;and of course, I will keep this feature. Ok, sorry. I should talk of the NEW stuff. Well, some additional librarys and programs: I am about to integrate less languages and therefore librarys like ncurses, I will try to integrate elinks, moon-buggy and maybe  (not sure yet) irssi. The main problem about additional software is, that I am unable to modify ezxterm to a more comfortable environment with a char-chooser (or call it keycode chooser) to get rid of the need to have an ascii code table in your pocket (or brain) and to be able to easily enter keycodes like F1-F12, as some programs require such input. 

Very recently, I had another idea: Changing the browsers identification. I put that on my list, being unsure wether it can be done, because I thought of changing it to Androids UA-string might change the look of some mobile services (especially those by Google, if you've tried the Android emulator, you might know that there is a difference). And even if the included Opera 8.5 isn't the best mobile browser on earth, it is not the worst either, I wouldn't even say it is one of the bad ones.
My list has to more points: VPNC (for my university, I will have to remember to remove my connection settings&#8230; ;) ) and little optimizations regarding presets (connections, email accounts, SIP). That's it.

And please don't ask for a day it will be ready: It's done, when it's done.* ;)

* But I hope to have it done before OpenEZX/FSO/Qt Extended is ready for A910.
