+++
title = "Openmoko - Plan B and Sean Moss-Pultz's talk from OpenExpo"
aliases = ["2009/04/06/openmoko-plan-b-and-sean-moss-pultzs-talk-from-openexpo.html"]
author = "peter"
date = "2009-04-06T12:37:00Z"
layout = "post"
[taxonomies]
tags = ["channels", "ESC", "marketing", "OpenExpo", "Openmoko", "Plan B", "platforms", "sales", "twitter", "Youtube"]
categories = ["commentary", "hardware",]
authors = ["peter"]
+++

_What we know is very little._ We know that Openmoko's &#8220;Plan B&#8221; Device will look like the thing the CEO of Openmoko Inc., Sean Moss-Pultz has in his right hand on the upper picture, and we know it won't be a phone. As I already guessed on saturday, it might be a MID or some kind of multimedia device&mdash;and it isn't unlikely that it's based on a Samsung SoC, e.g. the S3C6410, which was to be used in the GTA03, the FreeRunner follow up.

But let's get back to the video, which I really recommend you to watch, as Sean Moss-Pultz is very honest [in this video](https://www.youtube.com/watch?v=UFuwhPXYxxI).

On saturday I already knew that there was some cost difference, or let's say a difference in ressource usage between &#8220;Plan B&#8221; / &#8220;project b&#8221; and the FreeRunner successor, which was cancelled as it would have required 3 times the ressources of that device that is to be announced &#8220;soon&#8221;.

Now why this difference? Yes, it isn't the GSM chip only. But Openmoko was in fact planning to create a consumer product (after all the dissapointments with the FreeRunner) and this would have understandably used high ressources to be able to ship the phone with a really reliable phone stack and a basic set of applications. And trying to find carriers for the product and so on. Huge ressources.

In fact, many of us who want open feature phones wouldn't have been happy with that, what was planned, as GPS and WiFi were removed on the last iterations of GTA03, in fact I am almost happy that it was cancelled. Because actually I have to say that I don't believe in &#8220;openess&#8221; as huge selling point. Surely, Openmoko devices are open like no other in their market segment, but if i look at the people I know that don't pray to become more RMS-like every single day don't really care about &#8220;openess&#8221;. They rather care about other features (and of course, a phone has to be reliable first and for some have good PIM functions), like a camera (2MP of GTA03 is pretty boring), WiFi, GPS or 3.5G connectivity and I shouldn't forget to mention standby time.
Of course, Sales could try to do some campaigns, to get more people involved. Yes, this is a point I have to critisize on Openmoko Inc.: It's not only they need a person, which looks 7 days a week after the community, they need to communicate better. Today I have seen this engadget headline: 

<blockquote>
<p>&#8220;OpenMoko FreeRunner canceled, staff slashed&#8221;<p>

<cite><a href="https://www.engadget.com/2009-04-06-openmoko-freerunner-canceled-staff-slashed.html">Engadget</a></cite>
</blockquote>

I _like_ engadget, but this is just ridiculous, and it is an &#8220;error&#8221; based on a <a href="https://web.archive.org/web/20151001132158/http://translate.google.com/translate?prev=_t&hl=en&ie=UTF-8&u=http%3A%2F%2Fwww.bernerzeitung.ch%2Fdigital%2Fmobil%2FVorlaeufig-kein-weiteres-LinuxTelefon%2Fstory%2F15176367&sl=de&tl=en&history_state0=">bad article</a> by &#8220;Berner Zeitung&#8221; days ago. You absolutely have to avoid such wrong information, when some newspaper get's something wrong you have to contact them immidiately to make them fix that&mdash;otherwise this wrong information keeps on spreading. Well, I can easily go on: Why is there no corporate blog? Not that all corporate blogs are fantastic, but you need a communication platform and in times where Web 2.0 is already a grandma of buzzwords, an innovative company shouldn't miss this. And hey, why didn't Openmoko continue this <a href="http://gettingstartedopenmoko.wordpress.com/">__Getting started Openmoko__-Blog</a>? 

Though the name wasn't that good in my opinion (not catchy enough) and doing such a thing at a normal blog hoster is bad too, adding a twitter/identi.ca account and posting videos (own __YouTube channel__) regularily to show use cases, advantages of openess, news and so on would have been a good (and rather cheap) way to become known by more people. Not everybody is keen on mailing lists ;)

__It's dead simple: People have to know you and your product, because those who don't know you, won't buy your product!__ _(Ok, it isn't that simple to become well known or even popular&#8230; ;) )_

But there is hope that this will change, as Openmoko will hopefully work on getting more audience&mdash;after many people at _Embedded Systems Conference_ didn't know the FreeRunner, though it is some kind of a cheap and powerful embedded development board.

Well, enough of this, I will try to get back to Openmokos &#8220;Plan B&#8221; (which needs some better marketing than the units before, anyway). The device I would like to see the most would be a multimedia capable MID, with open software, capable to play OGG formats (hopefully not only them), as there are few OGG capable players on the market&mdash;one has to find a niche, as this market is crowded, too. Good campaigning, and such a device can be a success, THE success Openmoko needs to stay on the markets in long term.

__UPDATE:__ [A video statement by Openmoko Inc.'s VP of Marketing, Steve Mosher](https://www.youtube.com/watch?v=_d8Tsvj2TdQ).

&#8220;Announcement date of Project B in August&#8221;.

_Means we have lots of time for guesses ;)_
