+++
title = "Ubuntu Edge"
aliases = ["2013/07/24/ubuntu-edge"]
author = "peter"
date = "2013-07-24T11:37:00Z"
layout = "post"
[taxonomies]
tags = ["Bleeding Edge", "Convergence", "deutsch", "lapdock", "linmob", "platforms", "smartphones", "ubuntu", "Ubuntu Edge"]
categories = ["hardware", "shortform",]
authors = ["peter"]
+++
Marc Shuttleworth, the guy who created Ubuntu, announced a new project: A Smartphone that runs Ubuntu. It is a high-end smartphone, and is [crowd-funded](http://www.indiegogo.com/projects/ubuntu-edge).

[Watch the video on YouTube](https://www.youtube.com/embed/eQLe3iIMN7k)

This project is really interesting, but also quite expensive&mdash;I just can't spend USD 830,00 right now to get a new phone in May of 2014. That said, I appreciate the idea of Convergence. Motorola's [Lapdock](https://linmob.net/tags/lapdock) approach may have been to early in terms of SoC performance (additionally it did not help, that Android's no fun, when used with a trackpad)&mdash;but my ARM [Chromebook](https://linmob.net/tags/chromebook/) is (also running a light spin of Ubuntu (using LXDE as main UI) capable enough for my everyday tasks. The Ubuntu Edge is supposed to ship with 4GB of RAM and the best ship, that is going to available at the point in time when the design is going to be finished. Therefore, it's save to say, that these USD 830 buy more than just a smartphone.
If that happens: Collecting USD 32,000,000 via crowd funding would be a first. _Currently it's looking good though._

__More:__
* [A first Hands-On Video](http://www.youtube.com/watch?v=BYM7tCjp0Go)
* [Jason Hiner for cnet.com with thoughts on three aspects of the Ubuntu Edge](http://news.cnet.com/8301-1035_3-57595055-94/ubuntu-edge-3-things-to-think-about/)

