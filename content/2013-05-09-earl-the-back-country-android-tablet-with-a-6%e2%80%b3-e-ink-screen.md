+++
title = "Earl, the Back-Country Android Tablet With a 6\" E-ink Screen"
aliases = ["2013/05/09/earl-the-back-country-eink-tablet.html"]
author = "peter"
date = "2013-05-09T06:03:00Z"
layout = "post"
[taxonomies]
tags = ["dual core", "earl", "eInk", "eInk tablets", "fascinating niche products",]
categories = ["hardware"]
authors = ["peter"]
+++
_Sometimes you happen to stumble on products you have been waiting for._ Today, this happened to me once again. In fact, if I hadn't been so busy lately because of an exam (which went well, as far as I can tell), I might just have written a post asking why there are no E-Ink tablets or at least eReaders with a dual core chip and Android. Why? Well, eInk is great, (I really like the black and white look...) and my Nook Simple Touch is not only an eReader to me, it also serves as a simple Android tablet from time to time, as it is light and super portable (in comparison to iPad) and has a pretty decent battery life. But the Nook Simple Touch is still on "Eclair" or Android 2.1 (which runs nicely with the 600MHz ARMv7 and 256MB, but it's getting really old). And until I read <a href="http://www.the-digital-reader.com/2013/05/08/meet-earl-the-back-country-android-tablet-with-a-6-e-ink-screen/">this post at "The Digital Reader"</a> the only hope for a slightly better eInk-Tablet was the Tolino Shine, which ships with Android 2.3 and a higher-res eInk screen.

Now, as I write this, there is hope for such a product which can be significantly faster. [But head over to "The Digital Reader" and read the facts yourselves](http://www.the-digital-reader.com/2013/05/08/meet-earl-the-back-country-android-tablet-with-a-6-e-ink-screen/).

__SOURCE:__ <a href="http://www.the-digital-reader.com/2013/05/08/meet-earl-the-back-country-android-tablet-with-a-6-e-ink-screen/">The Digital Reader: Meet Earl, the Back-Country Android Tablet With a 6″ E-ink Screen</a>
