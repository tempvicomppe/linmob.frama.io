+++
title = "PinePhone DDC pt. 2: Flatpaks and Scaling"
aliases = ["2020/07/27/pinephone-daily-driver-challenge-part2-flatpak-and-scaling-in-phosh.html"]
author = "Peter"
date = "2020-07-27T05:33:18Z"
updated = "2022-05-29T17:17:00Z"
layout = "post"
[taxonomies]
tags = ["PinePhone", "Video content", "Daily Driver Challenge", "postmarketOS", "Arch Linux ARM", "Huong Tram Linux", "Calindori", "Flatpak", "Flathub", "Scaling", "Phosh"]
categories = ["howto"]
authors = ["peter"]
[extra]
updated_note = "Added 'Managing space' section."
+++
I have decided to change my approach: Writing the post before shooting the third "PinePhone as a daily driver" video. This week is going to be about Flatpak, Reading apps and the intricacies of running (non-)Flatpak Qt and GTK apps on the respective "other side". Part 1: Flatpak and Scaling in Phosh.
<!-- more -->

__Caveat:__ _For the past week I have been struggling with receiving calls on Phosh/ModemManager based distributions, approximately since the massive improvements in battery life due to new Crust firmware. I found another user suffering from this issue, but assuming my tweets are being read among PinePhone users, it does not seem to be a widespread problem. I am continuing with this series despite of these issues, but be aware that core functionality might be currently not available._


### Flatpaks across PinePhone distributions
As many of the apps I will discuss later on are first and foremosts available as Flatpaks, let's dive into tutorial territory on how to install flatpaks.
The first part of this is specific for your distribution:

#### postmarketOS
_I switched my eMMC OS to postmarketOS this week, using one of their images that include their installer._
If you are using _Phosh_, get Flatpak support by typing
`sudo apk add gnome-software-plugin-flatpak`,
if you insist on running _Plasma Mobile_ despite its early state run
`sudo apk add discover-backend-flatpak` instead. If you don't want Flatpak in your software center or you are among those, that just run another window manager, e.g. _sway_, just run
`sudo apk add flatpak`.

#### Arch and Manjaro
`sudo pacman -Syu flatpak` gets you bare minimum _flatpak_ support here. Arch does not come with sudo enabled by default, therefore a `su` and then a `pacman -Syu flatpak` should get the job done.

#### Mobian / PureOS port
`sudo apt install flatpak` for basic flatpak support, and `sudo apt install gnome-software-plugin-flatpak` (which depends on flatpak, so running this should be enough) gets you Flatpak support in Gnome Software Center.

#### Enabling Flathub

Just having Flatpak does not help you much. [Flathub](https://flathub.org) contains many mobile optimized and other apps, that may work with `scale-to-fit`. Just run `sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo` (or without the sudo, if you are root) and you will have Flathub and all its software available for installation.
If you want to install the software from the _Gnome Software Center_ or _Discover_, and it does not work yet, try a reboot.

#### Enabling other Flathub repositories, e.g. KDE Apps
Just run `flatpak remote-add --if-not-exists kdeapps --from https://distribute.kde.org/kdeapps.flatpakrepo` as root. 

#### Tradeoffs of using flatpak apps
Flatpaks have caused some controversy in the Linux community, and their security model has been [questioned](http://flatkill.org/). These quarrels aside, you need to know that using just a few Flatpaks will take _a ton of disk space on your phone_, as all the foundations for the app will be loaded with them &mdash; just don't use them on a 4GB or 8GB SDHC card. Also, as the base flatpaks need to be downloaded and updated, you should have enough bandwith. Unless your mobile data plan is truly unlimited, download them over a non-metered connection.

#### Installing Flatpaks
Now that you have been warned, installing Flatpaks is simple. You can do so from _Gnome Software_ or _Discover_, or just by running `flatpak install appname`. You do not need to know the exact name, `flatpak` will figure it out and let you choose between options, if there is more than one Flatpak matching your query. Just try it, it's simple!

##### Exercise: Installing the Cawbird twitter client
[Cawbird](https://github.com/IBBoard/cawbird) is a nice GTK-based Twitter client, that works on the PinePhone, but does not scale perfectly by default.
Type `flatpak install cawbird` and hit Enter
~~~~~~
Looking for matches…
Found similar ref(s) for ‘cawbird’ in remote ‘flathub’ (system).
Use this remote? [Y/n]:
Found ref ‘app/uk.co.ibboard.cawbird/aarch64/stable’ in remote ‘flathub’ (system).
Use this ref? [Y/n]:
~~~~~~
It really is simple, you just have to read and press Enter if you agree.

#### Managing space

While installing multiple apps that use the same set of base runtimes reduces the space of your installed flatpak apps when calculated per app, the space these flatpaks use may increase over time. To make sure you're not wasting too much of your precious disk space (or when you are getting warnings that your filesystem is almost at capacity), do run `flatpak repair` (in case of system-wide installed flatpaks make sure to run it with elevated permissions (as root or with `sudo`)). It might reduce the space installed flatpaks occupy on your system by quite a bit - it once freed 5 gigabytes on my Librem 5.


### Manual Scaling in Phosh
Now that we have installed an app, it may happen that it does not scale correctly. Cawbird is one of these apps that almost works out of the box and after telling Phosh to scale it properly, is just fine.

Newer Phosh distributions come with a command called `scale-to-fit`. It's used like this: `$ scale-to-fit <APP-ID> on`. For Cawbird it is simple: Just `$ scale-to-fit cawbird on`, as is documented on the Mobian Wiki. But let's assume we did not know it &mdash; and you will eventually run into this &mdash; how can we simply find out the APP-ID?

The method on the Mobian Wiki works, but it is clunky. Yesterday I scrolled through the blog of Phoshs main developer/release manager, and took what I found there to come up with the following simple bash-script:

~~~~~
#!/bin/bash
WAYLAND_DEBUG=1 $1 |& grep 'xdg_toplevel@[0-9]\+\.set_app_id'
~~~~~

Just copy these two lines into a text file, e.g. named `get-app-id.sh`, on your PinePhone and make it executable by running `$ chmod +x get-app-id.sh`.

Let's say, you want to use KeePassXC, which does not scale well, but becomes somewhat usable by running `scale-to-fit`. But you won't get there by running `$ scale-to-fit keepassxc on`. You can now run `$ ./get-app-id.sh keepassxc` and _KeePassXC_ will start up. Go back to your terminal and you will find a line of text similar to:

~~~~~~
[4049990.198] -> xdg_toplevel@21.set_app_id("org.keepassxc.KeePassXC")
~~~~~~

The part between the "" is what you are looking for. Just run `$ scale-to-fit org.keepassxc.KeePassXC` and the next time you start KeePassXC, it should be usable.

The same works for Flatpak apps. You will just need to find out the Flatpaks Application ID, which is possible by performing `$ flatpak search ...`, or for Flathub apps by just looking at the flathub.org webpage for the specific app you want to adjust, e.g. `https://flathub.org/apps/details/uk.co.ibboard.cawbird`, where _uk.co.ibboard.cawbird_ is what you need.

Now you can just run a `$ ./get-app-id.sh "flatpak run uk.co.ibboard.cawbird"` and that will return the APP-ID you are looking for:

~~~~~~
[536137.138] -> xdg_toplevel@29.set_app_id("cawbird")
~~~~~~

_Easy, huh? I hope this post was helpful, if you have questions, just contact me on [Twitter](https://twitter.com/linmobblog), [Mastodon](https://fosstodon.org/@linmob) or via e-mail._

__Addition (07/27/2020, 8:14pm CEST):__ There is another way to get the APP-ID that [has been pointed out to me by Twitter user @dos1](https://twitter.com/dos1/status/1287569070045507587):

> Compile `examples/foreign-toplevel.c` from wlroots source tree and you'll get a tool that lists you app-ids of open windows :P

This approach requires more effort once and then a lot less effort going forward. _Choose wisely!_
