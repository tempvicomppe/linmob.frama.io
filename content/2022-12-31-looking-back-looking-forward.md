+++
title = "Looking back, looking forward (2022/2023)"
date = "2022-12-31T14:30:00Z"
draft = false
[taxonomies]
tags = ["PinePhone Pro", "Librem 5", "SHIFT6mq", "GTK4", "Qt 6","postmarketOS","Phosh Ticket Box",]
categories = ["personal","commentary"]
authors = ["peter"]

+++

Time for another annoying "end of year"-post - prepare for a boring read!
<!-- more -->

### Looking back

To be frank: When I look back at 2022, I look back in disappointment at my output. I did not manage to deliver the promised "2 Years with PinePhone video", I did not manage to go through with a series of blog posts about the Librem 5 that I planned with my original October 23rd, 2017, pre-order-Librem 5 that arrived in March - in fact, I barely touched the thing. And while LinuxPhoneApps.org is technically a separate project, I did not nearly manage to add the number of apps I hoped to add.

Time and energy were just far too scarce, due to a new, demanding day-job, and also due to private matters - I don't want to go into detail, so let's just say [Re-evaluating Priorities, Section Two, Paragraph One](https://linmob.net/re-evaluating-priorities/#current-events-and-the-near-future) still applies - which, in many ways is good, considering the alternative scenario.

So, at the end of this year, I sit here with batteries depleted and have to summarize: Personally, 2022 really sucked.

But, this is not what you are here for, right? What happened in 2022 with regards to #linuxmobile? Well, I think it was a pretty good year overall. I think that the hype died down a bit, and whils likely all of us wished that Purism would have delivered all Librem 5 pre-orders and the PinePhone Pro would be "more ready", I think we can still say that progress was made - and lets not forget about SHIFT, who actively contributed to get their [SHIFT6mq supported in postmarketOS](https://wiki.postmarketos.org/wiki/SHIFT_SHIFT6mq_(shift-axolotl)) - awesome! 🥳 Plasma Mobile got a lot better and smoother, and so did Phosh: Gesture support is really nice, especially when fat-fingering the spacebar ;-)

With GNOME Shell on mobile, there's even a sleek new contender, I hope to see much of it merged into GNOME 44. Sxmo progressed, too - but I don't think I am the right person to comment on this (lack of time, see above). I am glad to see UBports finally getting close to a release of their 20.04 rebasing; and it's great to see other long running efforts like Nemo Mobile (IMHO, the most performant software on PinePhone) and Maemo Leste to be alive and kicking.

Sadly, even with the projects I use regularily, I struggle to name important features (aside from many apps moving to GTK4/libadwaita and Phosh Ticket Box which I use for my travels today)[^1], but that may be a good thing: Fixing bugs is often more important than adding "headline features". For me, mobile Linux has gotten a more pleasant to use through this year, and I want to thank everybody who put in the hard work to make all these improvements possible - you are my heroes of 2022!

### Looking forward

With the mention of GNOME Shell on mobile I've already given away something we can look forward to. I believe that the inclusion of Plasma Mobile's apps into the proper KDE Gear releases is a good thing, as it represents Plasma Mobile being an integral part of Plasma and makes it easier for packagers/distributions, is a strong signal.[^2] I am also fairly certain that Qt 6 is going to be mentioned a lot. Talking of Qt-based UIs, I am really curious what Ubuntu Touch will be able to do, now that they are finishing moving to a more modern base.

Personally, I really look forward to attending FOSDEM in Brussels and meeting tons of people I've been interacting with online - I've been wanting to go so often, but was never able to make it happen. Travel and accomodation are booked, but 2022 taught me to always expect the unforeseen. Let's hope I'll manage to go this year! (If not, maybe I'll manage some other Linux-y events.)

While I would like to promise more content and activity, let's be realistic: It's rather unlikely that I will have more time. Therefore, I am working on documenting the process to publish on this blog better, after having implemented an author taxonomy recently.[^3] Please get in touch and send your drafts - or better yet, create a [merge request](https://framagit.org/linmob/linmob.frama.io). There's one big drawback, though: I did not manage to get this page relisted on Bing in 2022 (my attempts where unsuccessful) - meaning: DuckDuckGo users won't find the content here.[^4] I'll attempt to fix this in 2023, unless it means to move everything to a new domain.

Then there's the Weekly Update: Despite feeling like busywork, I'll try to keep it going best I can - no promises though. I have high hopes for a good year with news devices - but I'll spare you my predictions.

### Conclusion

After all these words, I wish you a Happy New Year and us all a great, prosperous year for #LinuxMobile!


[^1]: This post is written on a train. At least for Phosh 0.22, [this fediverse post by Chris Vogel is still a necessary read to get ticket box to work for you](https://chrichri.ween.de/o/a4ee24e26d7a4991a868e5ad540849d1), especially if you know that replacing the `get` with `set` allows you to set the path you want to set. I could have also named [Beeper](https://www.beeper.com/), a Matrix-based, paid service that unites Discord, Whatsapp, Twitter, Slack, Telegram and even more services that I use, as it meant a major improvement to my "mobile linux experience", but this is not advertising hour, am I right?

[^2]: Also, totally selflessly, I hope it leads to proper packaging of more Plasma Mobile apps in KDE Neon, which I still use on my Surface Go 2 tablet - which admittedly mostly use with its keyboard attached - as a tablet-able mini-laptop.

[^3]: I still need to add it to all the posts, but if you scroll to the top on this post, you may notice that you can click the author name now - and small profile pages are possible, too.

[^4]: So much for "Microsoft ❤️ Linux", am I right 😂
