# linmob.net

This repository generates the page seen under [LINMOB.net](https://linmob.net).
It's hosted on Framagit these days, after being hosted on GitHub and subsequently [Uberspace](https://uberspace.de) before.

## Authors and collaborators welcome!

If you want to join the effort, please request to become a project member. After that, you'll be able to easily create merge requests for articles or other changes!

## To Do / Stuff I'd like to add that's not in the issue tracker

* comments (via fraangut, email template with posttitle or that private disqus clone),
* try to integrate (some or all) posts of @linmmobblog or @linmob@fosstodon.org with this blog (partially done),
* video hosting.

## Notes for myself (and others who care)

Run Zola site locally:
`$ zola serve`

